package helpers

import (
	"app/repository"
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

type AppRunParameters struct {
	PathToDataBase  string
	PathForScanning string
	IgnorePath      string
	IsIndexing      bool
	ReneBase        bool
	WithHashReport  bool
}

func WriteCsvReportAboutFiles(files []repository.File) error {
	resultFile, err := os.Create("report.csv")
	CheckError(err)

	writer := csv.NewWriter(resultFile)

	records := [][]string{}

	headLine := []string{"id", "name", "extension", "hash", "path", "size", "modificated_time"}

	records = append(records, headLine)

	for _, file := range files {
		row := []string{
			fmt.Sprintf("%d", file.Id), file.Name, file.Extension, file.Hash, file.Path, fmt.Sprintf("%d", file.Size), fmt.Sprintf("%d", file.ModificatedTime),
		}
		records = append(records, row)
	}

	writer.Comma = ';'
	err = writer.WriteAll(records)

	if err != nil {
		return err
	}

	writer.Flush()

	err = resultFile.Close()

	if err != nil {
		return err
	}

	return nil
}

func ParseParameters() AppRunParameters {
	var pathToBase, pathForScan, ignorePath string
	var isIndexing, renewBase, withHashReport bool
	flag.StringVar(&pathToBase, "b", "", "[Required] Path to database for indexing")
	flag.StringVar(&pathForScan, "p", ".", "[Optional] Default: \".\". Path for scanning files")
	flag.StringVar(&ignorePath, "ignore", ".git", "[Optional] Default: \".git\". Ignore dir or file name or regexp")
	flag.BoolVar(&isIndexing, "i", false, "[Optional] Default: false. Run programm for indexing, if it false programm will run for check")
	flag.BoolVar(&renewBase, "renew", false, "[Optional] Default: false. Programm delete table and create new")
	flag.BoolVar(&withHashReport, "wh", false, "[Optional] Default: false. Report with file hash")

	flag.Parse()

	absPath, err := filepath.Abs(pathForScan)
	CheckError(err)

	pathForScan = absPath

	return AppRunParameters{
		PathToDataBase:  pathToBase,
		PathForScanning: pathForScan,
		IsIndexing:      isIndexing,
		ReneBase:        renewBase,
		WithHashReport:  withHashReport,
		IgnorePath:      ignorePath,
	}
}

func CheckError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
