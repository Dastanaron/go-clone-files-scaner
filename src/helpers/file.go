package helpers

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
)

func GetMD5SumByPath(path string) (*string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	hasher := md5.New()
	_, err = io.Copy(hasher, file)

	if err != nil {
		log.Fatal()
		return nil, err
	}

	file.Close()

	md5Sum := fmt.Sprintf("%x", hasher.Sum(nil)) //string(hasher.Sum(nil))

	return &md5Sum, nil
}
