package main

import (
	"app/helpers"
	"app/repository"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"regexp"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

func indexing(repo repository.FileRepository, pathForScanning string, ignorePath string) {

	files, err := ioutil.ReadDir(pathForScanning)

	helpers.CheckError(err)

	for _, file := range files {
		filePath := fmt.Sprintf("%s/%s", pathForScanning, file.Name())

		matched, _ := regexp.MatchString(ignorePath, filePath)
		if matched {
			continue
		}

		if !file.IsDir() {
			hashFileSum, err := helpers.GetMD5SumByPath(filePath)

			if err != nil {
				fmt.Println("Cannot compute hashsum: ", err.Error())
				continue
			}

			fileEntity := repository.File{
				Name:            file.Name(),
				Extension:       path.Ext(file.Name()),
				Size:            file.Size(),
				Hash:            *hashFileSum,
				Path:            filePath,
				ModificatedTime: file.ModTime().Unix(),
			}

			foundedFiles, err := repo.LoadByHash(*hashFileSum)
			helpers.CheckError(err)

			if file.Name() == "Курс от Мартенса!.pdf" {
				fmt.Println(foundedFiles)
			}

			repo.SetDefaultTable(repository.FILES_TABLE_NAME)
			_, err = repo.InsertFile(&fileEntity)
			helpers.CheckError(err)

			if len(foundedFiles) != 0 {
				repo.SetDefaultTable(repository.CLONES_TABLE_NAME)
				_, err = repo.InsertFile(&fileEntity)
				helpers.CheckError(err)
			}

			json, _ := json.Marshal(fileEntity)

			fmt.Println(string(json))
		} else {
			indexing(repo, filePath, ignorePath)
		}
	}
}

func report(repo repository.FileRepository, pathForScanning string, withHash bool) {
	files, err := repo.LoadDublesByPath(pathForScanning)
	helpers.CheckError(err)

	fmt.Println("Report about doubles")
	fmt.Println("--------------------")

	for _, file := range files {
		if withHash {
			fmt.Println(file.Path, file.Hash)
		} else {
			fmt.Println(file.Path)
		}

	}

	fmt.Println("--------------------")
	fmt.Println("Count clone files: ", len(files))
	fmt.Println("--------------------")

	helpers.WriteCsvReportAboutFiles(files)
}

func main() {
	startTime := time.Now()
	appParameters := helpers.ParseParameters()

	sqliteConnectionParameters := fmt.Sprintf("file:%s?cache=shared", appParameters.PathToDataBase)

	db, err := sql.Open("sqlite3", sqliteConnectionParameters)
	helpers.CheckError(err)

	fileRepository := repository.GetRepo(db)

	if appParameters.ReneBase && appParameters.IsIndexing {
		fileRepository.RenewTable()
	}

	if appParameters.IsIndexing {
		indexing(fileRepository, appParameters.PathForScanning, appParameters.IgnorePath)
	}

	report(fileRepository, appParameters.PathForScanning, appParameters.WithHashReport)

	fmt.Println("--------------------")
	fmt.Println("Complete time:", time.Since(startTime))
	fmt.Println("--------------------")
}
