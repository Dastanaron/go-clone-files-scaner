package repository

import (
	"database/sql"
	"fmt"
)

type File struct {
	Id              int64
	Name            string
	Path            string
	Hash            string
	Size            int64
	Extension       string
	ModificatedTime int64
}

type FileRepository struct {
	db            *sql.DB
	DEFAULT_TABLE string
}

type DBInfo struct {
	LastInsertId int64
	AffectedRows int64
}

const FILES_TABLE_NAME = "files"
const CLONES_TABLE_NAME = "clones"

func GetRepo(dbConnection *sql.DB) FileRepository {
	return FileRepository{
		db:            dbConnection,
		DEFAULT_TABLE: FILES_TABLE_NAME,
	}
}

func (fr *FileRepository) SetDefaultTable(tableName string) {
	fr.DEFAULT_TABLE = tableName
}

func (fr *FileRepository) SetDb(dbConnection *sql.DB) {
	fr.db = dbConnection
}

func (fr *FileRepository) InsertFile(file *File) (dbInfo DBInfo, err error) {
	query := fmt.Sprintf("replace into %s (name, size, extension, path, hash, modificated_time) values (?, ?, ?, ?, ?, ?)", fr.DEFAULT_TABLE)

	stmt, err := fr.db.Prepare(query)

	if err != nil {
		return
	}

	res, err := stmt.Exec(file.Name, file.Size, file.Extension, file.Path, file.Hash, file.ModificatedTime)

	if err != nil {
		return
	}

	lastInsertId, err := res.LastInsertId()

	if err != nil {
		return
	}

	affectedRows, err := res.RowsAffected()

	if err != nil {
		return
	}

	dbInfo = DBInfo{
		LastInsertId: lastInsertId,
		AffectedRows: affectedRows,
	}

	file.Id = lastInsertId

	return
}

func (fr *FileRepository) LoadFiles() (files []File, err error) {
	query := fmt.Sprintf("select id, name, extension, size, modificated_time, path, hash from %s", fr.DEFAULT_TABLE)

	rows, err := fr.db.Query(query)

	if err != nil {
		return
	}

	for rows.Next() {
		var row File
		err = rows.Scan(&row.Id, &row.Name, &row.Extension, &row.Size, &row.ModificatedTime, &row.Path, &row.Hash)
		if err != nil {
			return
		}
		files = append(files, row)
	}

	return
}

func (fr *FileRepository) LoadByHash(hash string) (files []File, err error) {
	query := fmt.Sprintf(`
	select 
	id, name, extension, size, modificated_time, path, hash 
	from %s 
	where hash = "%s"`, FILES_TABLE_NAME, hash)

	rows, err := fr.db.Query(query)

	if err != nil {
		return
	}

	for rows.Next() {
		var row File
		err = rows.Scan(&row.Id, &row.Name, &row.Extension, &row.Size, &row.ModificatedTime, &row.Path, &row.Hash)
		if err != nil {
			return
		}
		files = append(files, row)
	}

	return
}

func (fr *FileRepository) LoadDublesByPath(path string) (files []File, err error) {
	if path == "" {
		path = "/"
	}

	path = fmt.Sprintf("%s%s", path, "%")

	query := fmt.Sprintf(`select 
	f.id, f.name, f.extension, f.size, f.modificated_time, f.path, f.hash 
	from 
		%s as f 
	where 
		hash in(
		select 
			distinct d.hash 
		from 
			%s as d
		where 
			d.path like "%s"
		) 
	order by 
	f.hash asc`, FILES_TABLE_NAME, CLONES_TABLE_NAME, path)

	rows, err := fr.db.Query(query)

	if err != nil {
		return
	}

	for rows.Next() {
		var row File
		err = rows.Scan(&row.Id, &row.Name, &row.Extension, &row.Size, &row.ModificatedTime, &row.Path, &row.Hash)
		if err != nil {
			return
		}
		files = append(files, row)
	}

	return
}

func (fr *FileRepository) CreateTables() {
	query := fmt.Sprintf(`CREATE TABLE "%s" (
		"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"name"	TEXT NOT NULL,
		"extension"	TEXT NOT NULL,
		"hash"	TEXT NOT NULL,
		"path"	TEXT NOT NULL,
		"size"	INTEGER NOT NULL,
		"modificated_time"	INTEGER
	)`, FILES_TABLE_NAME)

	fr.db.Exec(query)

	query = fmt.Sprintf(`CREATE TABLE "%s" (
		"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		"name"	TEXT NOT NULL,
		"extension"	TEXT NOT NULL,
		"hash"	TEXT NOT NULL,
		"path"	TEXT NOT NULL,
		"size"	INTEGER NOT NULL,
		"modificated_time"	INTEGER
	)`, CLONES_TABLE_NAME)
	fr.db.Exec(query)
}

func (fr *FileRepository) RenewTable() {
	query := fmt.Sprintf(`drop table if exists "%s"`, FILES_TABLE_NAME)
	fr.db.Exec(query)
	query = fmt.Sprintf(`drop table if exists "%s"`, CLONES_TABLE_NAME)
	fr.db.Exec(query)
	fr.CreateTables()
}
